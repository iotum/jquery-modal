# jQuery Modal

A simple & lightweight method of displaying modal windows with jQuery.

Based on [jQuery Modal from kylefox](https://raw.githubusercontent.com/kylefox/jquery-modal)

Designed for use in FreeConference.