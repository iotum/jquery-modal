/*
    A simple jQuery Modal
    Originally based on jQuery Modal by @kylefox at https://github.com/kylefox/jquery-modal/tree/v0.5.5
    Version 1.1.4
*/
(function($) {

  var current = null;

  $.modal = function(el, options) {
    $.modal.close(); // Close any open modals.
    var remove, target;
    this.$body = $('body');
    this.options = $.extend({}, $.modal.defaults, options);
    this.options.doFade = !isNaN(parseInt(this.options.fadeDuration, 10));
    this.$blocker = $('<div class="modal"><div class="modal__window"></div></div>');
    this.$blocker.addClass(this.options.modalClass);
    if (this.options.maxWidth) this.$blocker.find('.modal__window').css({maxWidth: this.options.maxWidth});
    if (el.is('a')) {
      target = el.attr('href');
      //Select element by id from href
      if (/^#/.test(target)) {
        this.$elm = $(target);
        if (this.$elm.length !== 1) return null;
        this.open();
      //AJAX
      } else {
        this.$elm = $('<div>');
        this.$body.append(this.$elm);
        remove = function(event, modal) { modal.elm.remove(); };
        this.showSpinner();
        el.trigger($.modal.AJAX_SEND);
        $.get(target).done(function(html) {
          if (!current) return;
          el.trigger($.modal.AJAX_SUCCESS);
          current.$elm.empty().append(html).on($.modal.CLOSE, remove);
          current.hideSpinner();
          current.open();
          el.trigger($.modal.AJAX_COMPLETE);
        }).fail(function() {
          el.trigger($.modal.AJAX_FAIL);
          current.hideSpinner();
          el.trigger($.modal.AJAX_COMPLETE);
        });
      }
    } else {
      this.$elm = el;
      this.open();
    }
  };

  $.modal.prototype = {
    constructor: $.modal,

    open: function() {
      this.block();
      this.show();
      if (this.options.escapeClose) {
        $(document).on('keydown.modal', function(event) {
          if (event.which == 27) {
            $.modal.close();
          }
        });
      }
      if (this.options.clickClose) {
        this.$blocker.click(function(event) {
          if ($(event.target).hasClass('modal')) {
            $.modal.close();
          }
        });
      }
      if (window.cordova) {
        document.addEventListener('backbutton', $.modal.close, false);
        if (window.StatusBar && window.innerWidth < 768) {
          StatusBar.styleDefault();
        }
      }
    },

    close: function() {
      this.unblock();
      this.hide();
      $(document).off('keydown.modal');
      if (window.cordova) {
        document.removeEventListener('backbutton', $.modal.close, false);
        // If in call, return style to light content
        if (window.StatusBar && window.innerWidth < 768 && $('body.call').length) {
          StatusBar.styleLightContent();
        }
      }
    },

    block: function() {
      this.$elm.trigger($.modal.BEFORE_BLOCK, [this._ctx()]);
      this.$body.addClass('no-scroll');
      this.$body.append(this.$blocker);
      this.$elm.trigger($.modal.BLOCK, [this._ctx()]);
    },

    unblock: function() {
      var _this = this;
      if(this.options.doFade) {
        setTimeout(function() {
          _this.$blocker.remove();
          _this.$body.removeClass('no-scroll');
        }, this.options.fadeDuration);
      } else {
        _this.$blocker.remove();
        this.$body.removeClass('no-scroll');
      }
    },

    show: function() {
      var _this = this;
      var content = this.$elm.clone();
      this.$elm.trigger($.modal.BEFORE_OPEN, [this._ctx()]);
      if (this.options.showClose) {
        this.closeButton = $('<a href="#close-modal" rel="modal:close" class="modal__close ' + this.options.closeClass + '"><i class="icon icon-remove"></i></a>');
        this.$blocker.find('.modal__window').append(this.closeButton);
      }
      this.$blocker.find('.modal__window').append(content);
      this.$blocker.find(content).show();
      setTimeout(function() {
        _this.$blocker.addClass('active');
      }, 50);
      this.$elm.trigger($.modal.OPEN, [this._ctx()]);
    },

    hide: function() {
      this.$elm.trigger($.modal.BEFORE_CLOSE, [this._ctx()]);
      this.$blocker.removeClass('active');
      this.$elm.trigger($.modal.CLOSE, [this._ctx()]);
    },

    showSpinner: function() {
      if (!this.options.showSpinner) return;
      this.spinner = this.spinner || $('<div class="' + this.options.modalClass + '-spinner"></div>')
        .append(this.options.spinnerHtml);
      this.$body.append(this.spinner);
      this.spinner.show();
    },

    hideSpinner: function() {
      if (this.spinner) this.spinner.remove();
    },

    //Return context for custom events
    _ctx: function() {
      return { elm: this.$elm, blocker: this.$blocker, options: this.options };
    }
  };

  $.modal.close = function(event) {
    if (!current) return;
    if (event) event.preventDefault();
    current.close();
    var that = current.$elm;
    current = null;
    return that;
  };

  // Returns if there currently is an active modal
  $.modal.isActive = function () {
    return current ? true : false;
  }

  $.modal.defaults = {
    escapeClose: true,
    clickClose: true,
    closeClass: '',
    modalClass: 'modal--light',
    spinnerHtml: null,
    showSpinner: true,
    showClose: true,
    fadeDuration: 300,   // Number of milliseconds the fade animation takes.
    maxWidth: null
  };

  // Event constants
  $.modal.BEFORE_BLOCK = 'modal:before-block';
  $.modal.BLOCK = 'modal:block';
  $.modal.BEFORE_OPEN = 'modal:before-open';
  $.modal.OPEN = 'modal:open';
  $.modal.BEFORE_CLOSE = 'modal:before-close';
  $.modal.CLOSE = 'modal:close';
  $.modal.AJAX_SEND = 'modal:ajax:send';
  $.modal.AJAX_SUCCESS = 'modal:ajax:success';
  $.modal.AJAX_FAIL = 'modal:ajax:fail';
  $.modal.AJAX_COMPLETE = 'modal:ajax:complete';

  $.fn.modal = function(options){
    if (this.length === 1) {
      current = new $.modal(this, options);
    }
    return this;
  };

  // Automatically bind links with rel="modal:close" to, well, close the modal.
  $(document).on('click.modal', '[rel="modal:close"]', $.modal.close);
  $(document).on('click.modal', '[rel="modal:open"]', function(event) {
    event.preventDefault();
    $(this).modal();
  });
})(jQuery);